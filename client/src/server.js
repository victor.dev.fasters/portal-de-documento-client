import { createServer, Model, Response } from "miragejs"

export function makeServer({ environment = "development" } = {}) {
    createServer({
        environment,

        models: {
            user: Model,
        },

        seeds(server) {
            server.create("user", { email: "teste@teste.com.br", password: "123456" })
        },

        routes() {
            this.namespace = "api"

            this.post("/login", (schema, request) => {
                const { email, password } = JSON.parse(request.requestBody)

                const userLogin = schema.users.findBy({email, password})

                if(userLogin) return new Response(200, { some: 'header' }, { message: "Ok" });

                return new Response(404, { some: 'header' }, { message: "Falha na autenticação" });
            })
        },
    })
}